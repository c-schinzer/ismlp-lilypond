\paper {
  #(include-special-characters)
}

\header {
  title = "Es ist ein Ros entsprungen"
  poet = "Text: (1-2) Trier 1587/88, (3-5) Friedr. Layriz 1844 "
  composer = "Melodie: Köln 1599, Satz: Michael Praetorius 1609"
}

timeline = {
  \time 4/4
  \partial 2
  s2 | s1 | s1 | s1 | s1 | s2 \breathe
  s2 | s1 | s1 | s1 | s1 | s2
  s2 | s1 | s2
  s2 | s1 | s1 | s1 | s1 \fine
}

global = {
  \key f \major
}

soprano =   \relative c'' {
  c2 c4 c d c c2 a bes a4 g (g) f2 e4 f2
  c' c4 c d c c2 a bes a4 g (g) f2 e4 f2 
  r4 a4 g e f d c2
  r4 c' c c d c c2 a bes a4 g (g) f2 e4 f1
}

alto =   \relative c'' {
  a2 a4 f f f e2 d d c d4. a8 c2 c
  a' a4 f f f e2 d d c d4. a8 c2 c
  r4 f d c c b c8 (d e4)
  r4 e g f f f e2 d d f4 d e (f g) c, c1
}

tenor =   \relative c' {
  c2 c4 a bes a g2 f f a4 c bes (a2) g4 a2
  c2 c4 a bes a g2 f f a4 c bes (a2) g4 a2
  r4 c bes a a g g2
  r4 g g a bes a g2 fis g c4 bes (a2) g2 a1
}

bass =   \relative c {
  f2 f4 f bes f c2 d bes f'4 e d2 c f,
  f'2 f4 f bes f c2 d bes f'4 e d2 c f,
  r4 f' g a f g c,2
  r4 c e f bes, f' c2 d g, a4 bes c2 c f,1
}

textOne = \lyricmode {
  Es ist ein Ros ent -- sprun -- gen aus ei -- ner Wur -- zel zart,
  wie uns die Al -- ten sun -- gen, von Jes -- se kam die Art
  und hat ein Bl&uuml;m -- lein bracht
  mit -- ten im kal -- ten Win -- ter wohl zu der hal -- ben Nacht.
}

textTwo = \lyricmode {
  Das Rös -- lein, das ich mei -- ne, da -- von Je -- sa -- ia sagt,
  Hat uns ge -- bracht al -- lei -- ne Ma -- rie, die rei -- ne Magd.
  Aus Got -- tes ew -- gem Rat
  hat sie ein Kind ge -- bo -- ren, wel -- ches uns se -- lig macht.
}

textThree = \lyricmode {
  Das Bl&uuml; -- me -- lein so klei -- ne, das duf -- tet uns so s&uuml;&ss;,
  Mit sei -- nem hel -- len Schei -- ne ver -- treibts die Fin -- ster -- nis.
  Wahr Mensch und wah -- rer Gott;
  hilft uns aus al -- lem Lei -- de, ret -- tet von Sünd und Tod.
}

textFour = \lyricmode {
  Lob, Ehr sei Gott dem Va -- ter, dem Sohn und heil -- gen Geist!
  Ma -- ria, Got -- tes -- mut -- ter, sei hoch ge -- be -- ne -- deit!
  Der in der Krip -- pen lag,
  der wen -- det Got -- tes Zo -- ren, wan -- delt die Nacht in Tag.
}

textFive = \lyricmode {
  O Je -- su, bis zum Schei -- den aus die -- sem Jam -- mer -- tal
  La&ss; dein Hilf uns ge -- lei -- ten hin zu den Freu -- den -- saal,
  In dei -- nes Va -- ters Reich,
  da wir dich e -- wig lo -- ben: o Gott, uns das ver -- leih!
}

\score {  % Start score
  <<
    \new ChoirStaff <<  % Start choirstaff
      \new Staff <<  % Start Staff = women
        \global
        \clef "treble"
        \new Voice = "Soprano" <<  % Start Voice = "Soprano"
          \timeline
          \voiceOne
          \soprano
          \addlyrics {
            \set stanza = "1. " \textOne
          }
          \addlyrics {
            \set stanza = "2. " \textTwo
          }
          \addlyrics {
            \set stanza = "3. " \textThree
          }
          \addlyrics {
            \set stanza = "4. " \textFive
          }
        >>  % End Voice = "Soprano"
        \new Voice = "Alto" <<  % Start Voice = "Alto"
          \timeline
          \voiceTwo
          \alto
        >>  % End Voice = "Alto"
      >>  % End Staff = women
      \new Staff <<  % Start Staff = men
        \global
        \clef "bass"
        \new Voice = "Tenor" <<  % Start Voice = "Tenor"
          \timeline
          \voiceOne
          \tenor
        >>  % End Voice = "Tenor"
        \new Voice = "Bass" <<  % Start Voice = "Bass"
          \timeline
          \voiceTwo
          \bass
        >>  % End Voice = "Bass"
      >>  % End Staff = men
    >>  % End choirstaff
  >>
}  % End score
