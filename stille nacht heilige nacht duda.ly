\paper {
  #(include-special-characters)
}

\header {
  title = "Stille Nacht, heilige nacht"
  subtitle = "für Gemeinde, Oberchor und Orgel"
  poet = "Text: Joseph Mohr"
  composer = "Melodie: Franz Gruber 24.12.1818, Satz: Jörg Duda 8.12.1994"
}

timeline = {
  \time 6/8
  s2. | s2. | s2. | s2. \break
  \repeat volta 3 {
    s2. | s2. \breathe s2. | s2. \breathe
    s2. | s2. \breathe s2. | s2. \breathe
    s2. | s2. \breathe s2. | s2. \breathe
  }
}

global = {
  \key bes \major
}

melody =   \relative c'' {
  r2. r r r
  f8. (g16) f8 d4. f8. g16 f8 d4.
  c4 c8 a4. bes4 bes8 f4.
  g4 g8 bes8. a16 g8 f8. g16 f8 d4.
  g4 g8 bes8. a16 g8 f8. g16 f8 d4.
  c'4 c8 es8. c16 a8 bes4. (d)
  bes8 (f) d f8. es16 c8 bes4. (bes)
}

soprano =   \relative c'' {
  r2. r r r
  r4. bes8 (f) d' bes4 r8 bes f d' c4 r8
  c (f,) es' d4 r8 f (d) bes g4 r8
  g'4 es8 d8. es16 f8 bes,8. c16 d8 g,4 r8
  g'4 es8 d8. es16 f8 bes,8. c16 d8 g,4 r8
  a (c) es d es f g4.
  f4 bes, c g a bes4. r
}

alto =   \relative c' {
  r2. r r r
  r4. d4 f8 d4 r8 d8 bes f' es4 r8
  es c a' bes4 r8 f4 f8 es4 r8
  g4 bes8 bes8. bes16 f8 g8. g16 d8 es4 r8
  g4 bes8 bes8. bes16 f8 d8. es16 f8 es4 r8
  es c a' bes bes bes bes4.
  bes4 d,8 es d es d4. r
}

tenor =   \relative c' {
  r2. r r r
  r4. f,8. g16 f8 bes4 r8 f8. g16 f8 a4 r8
  a8. (bes16) c8 bes4 r8 bes4 bes8 bes4 r8
  bes4 es8 f d c bes g as bes4 r8
  bes4 es8 f d c bes f bes c4 r8
  c (a) c bes c d d4.
  d4 f8 a, bes c f,4. r
}

bass =   \relative c {
  r2. r r r
  bes4. bes bes4 r8 bes d bes f'4 r8
  f8. g16 f8 bes, r8 d4 d8 es4 r8
  es4 g8 bes (bes) a g bes, f' es4 r8
  es4 g8 bes bes a g d bes es4 r8
  f4 f8 g g f e4. f4 f8 f f, f bes4. r
}

pianoLh = \relative c' {
  <d, f>8. <es, g>16 <d, f>8 
}

textOne = \lyricmode {
  Stil -- le Nacht, hei -- li -- ge Nacht,
  Al -- les schläft ein -- sam wacht
  nur das trau -- te hoch -- hei -- li -- ge Paar
  Hol -- der Kna -- be im lok -- ki -- gen Haar,
  schlaf in himm -- li -- scher Ruh,
  schlaf in himm -- li -- scher Ruh!
}

textTwo = \lyricmode {
  Stil -- le Nacht, hei -- li -- ge Nacht,
  Hir -- ten erstt kund -- ge -- macht
  durch der Eng -- el Ha -- le -- lu -- ja
  t&ouml;nt es laut von fern und nah,
  Christ der Ret -- ter ist da,
  Christ der Ret -- ter ist da!
}

textThree = \lyricmode {
  Stil -- le Nacht, hei -- li -- ge Nacht,
  Got -- tes Sohn, o wie lacht
  lieb aus dei -- nem g&ouml;tt -- li -- chen Mund,
  da uns schl&auml;gt die ret -- ten -- de Stund,
  Christ in dei -- ner Ge -- burt,
  Christ in dei -- ner Ge -- burt!
}

\score {  % Start score
  <<
    \new Staff <<  % Start Staff = melody
      \global
      \clef "treble"
      \new Voice = "Melody" <<  % Start Voice = "Melody"
        \timeline
        \melody
        \addlyrics {
          \set stanza = "1. " \textOne
        }
        \addlyrics {
          \set stanza = "2. " \textTwo
        }
        \addlyrics {
          \set stanza = "3. " \textThree
        }
      >>  % End Voice = "Melody"
    >>  % End Staff = melody
    \new ChoirStaff <<  % Start choirstaff
      \new Staff <<  % Start Staff = women
        \global
        \clef "treble"
        \new Voice = "Soprano" <<  % Start Voice = "Soprano"
          \timeline
          \voiceOne
          \soprano
          \addlyrics {
            \set stanza = "1. " \textOne
          }
          \addlyrics {
            \set stanza = "2. " \textTwo
          }
          \addlyrics {
            \set stanza = "3. " \textThree
          }
        >>  % End Voice = "Soprano"
        \new Voice = "Alto" <<  % Start Voice = "Alto"
          \timeline
          \voiceTwo
          \alto
        >>  % End Voice = "Alto"
      >>  % End Staff = women
      \new Staff <<  % Start Staff = men
        \global
        \clef "bass"
        \new Voice = "Tenor" <<  % Start Voice = "Tenor"
          \timeline
          \voiceOne
          \tenor
        >>  % End Voice = "Tenor"
        \new Voice = "Bass" <<  % Start Voice = "Bass"
          \timeline
          \voiceTwo
          \bass
        >>  % End Voice = "Bass"
      >>  % End Staff = men
    >>  % End choirstaff
  >>
}  % End score
