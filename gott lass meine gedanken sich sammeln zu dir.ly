\paper {
  #(include-special-characters)
}

\header {
  title = "Gott, lass meine Gedanken sich sammeln zu dir"
  poet = "Text: Dietrich Bonhoeffer"
  composer = "Musik: Taize Community"
}

soprano =   \relative c' {
  \time 4/4
  \partial 4
  \key e \major
  \repeat volta 2 {
    e4 e8 dis cis b e4 e8 e fis fis4 gis8 e4 r8
    e8 a4 a8 gis b2 fis8 e fis gis gis4 r8
    e8 b'4 b8 b cis4 cis8 cis cis cis b a gis2
    cis,8 cis fis e gis b a gis fis4 fis8 fis gis4
    fis (fis8) cis e (fis a4.) gis8 gis2 r4
  }
}

alto =   \relative c' {
  \time 4/4
  \partial 4
  \key e \major
  \repeat volta 2 {
    b4 b8 b a b cis4 cis8 cis dis dis4 dis8 b4 r8
    e8 e4 e8 e dis2 cis8 cis cis cis dis4 r8
    e8 e4 e8 e e4 e8 e fis fis dis dis e2
    cis8 cis fis e dis dis e e cis4
    b8 b e4 cis (cis8) cis cis4 (e4.) e8 e2 r4
  }
}

tenor =   \relative c' {
  \time 4/4
  \partial 4
  \key e \major
  \clef "treble_8"
  \repeat volta 2 {
    gis4 gis8 gis a a gis4 gis8 gis b b4 a8 gis4 r8
    gis8 cis4 cis8 cis b2 a8 a a a gis4 r8
    gis8 b4 b8 gis gis4 gis8 gis a a fis b b2
    cis8 b cis cis b b cis b a4
    b8 b b4 ais gis a2 cis4 b2 r4
  }
}

bass =   \relative c {
  \time 4/4
  \partial 4
  \key e \major
  \clef bass
  \repeat volta 2 {
    e4 e8 e fis dis e4 cis8 cis b b4 b8 e4 r8
    e8 cis4 e8 e gis2 a8 e dis cis gis'4 r8
    gis8 e4 fis8 gis cis,4 cis8 cis a a b b e2
    cis'8 b a a gis fis e e fis4
    dis8 dis e4 fis gis a (cis,) a e'2 r4
  }
}

text = \lyricmode {
  Gott, lass mei -- ne Ge -- dan -- ken sich sam -- meln zu dir,
  Bei dir ist das Licht, du ver -- gisst mich nicht.
  Bei dir ist die Hil -- fe, bei dir ist die Ge -- duld.
  Ich ver -- ste -- he dei -- ne We -- ge nicht,
  a -- ber du wei&ss;t den Weg f&uuml;r mich.
}

\score \transpose e f {
  <<
    \new Staff {
      \new Voice = "Soprano" {
        \soprano
      }
    }
    \new Lyrics {
      \lyricsto "Soprano"
      \text
    }

    \new Staff {
      \new Voice = "Alto" {
        \alto
      }
    }
    \new Lyrics {
      \lyricsto "Alto"
      \text
    }

    \new Staff {
      \new Voice = "Tenor" {
        \tenor
      }
    }
    \new Lyrics {
      \lyricsto "Tenor"
      \text
    }

    \new Staff {
      \new Voice = "Bass" {
        \bass
      }
    }
    \new Lyrics {
      \lyricsto "Bass"
      \text
    }
  >>
  }
  
